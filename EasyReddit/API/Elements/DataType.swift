//
//  Types.swift
//  reddit
//
//  Created by Lucas Sas on 12.08.19.
//  Copyright © 2019 Lucas Sas. All rights reserved.
//

import Foundation

public class DataType {
    public let kind: String
    
    
    init(kind: String) {
        self.kind = kind
    }
}
