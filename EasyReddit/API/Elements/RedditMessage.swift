//
//  Message.swift
//  reddit
//
//  Created by Lucas Sas on 13.08.19.
//  Copyright © 2019 Lucas Sas. All rights reserved.
//

import Foundation


public class RedditMessage: DataType {
    public static let kind: String = "t4"
}
