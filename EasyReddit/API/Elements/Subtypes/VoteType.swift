//
//  VoteType.swift
//  EasyReddit
//
//  Created by Lucas Sas on 04.09.19.
//  Copyright © 2019 Lucas Sas. All rights reserved.
//

import Foundation

public enum VoteType {
    case Upvote
    case Downvote
    case Neutral
}
